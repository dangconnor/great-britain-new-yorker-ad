(function() {

function preInit() {
  setupDom();

  if (Enabler.isInitialized()) {
    init();
  } else {
    Enabler.addEventListener(
      studio.events.StudioEvent.INIT,init);
  }
}

// Runs when Enabler is ready.
function init() {
  Enabler.setStartExpanded(false);
  addListeners();

  // Polite loading
  if (Enabler.isPageLoaded()) {
    politeInit();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, politeInit);
  }
};

function addListeners() {
  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_START, expandStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.EXPAND_FINISH, expandFinishHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_START, collapseStartHandler);
  Enabler.addEventListener(studio.events.StudioEvent.COLLAPSE_FINISH, collapseFinishHandler);
  creative.dom.expandButton.addEventListener('click', onExpandHandler, false);
  creative.dom.collapseButton.addEventListener('click', onCollapseClickHandler, false);
  creative.dom.expandedExit.addEventListener('click', exitClickHandler);
  creative.dom.collapsedExit.addEventListener('click', collapsedExitClickHandler);
}



function expandStartHandler() {
  // Show expanded content.
  //creative.dom.expandedContent.style.display = 'block';
  creative.dom.expandedContent.style.width = '600px';
  creative.dom.expandedContent.style.left = '0px';
  creative.dom.expandedExit.style.display = 'block';
  creative.dom.collapseButton.style.display = 'block';
 // creative.dom.collapsedContent.style.display = 'none';
  creative.dom.collapsedExit.style.display = 'none';
  creative.dom.expandButton.style.display = 'none';
  Enabler.finishExpand();
}              


var hasShifted = false;
var fyor_text_findX =  "38px";

function collapseStartHandler() {
  // Perform collapse animation.
 // creative.dom.expandedContent.style.display = 'none';
  creative.dom.expandedContent.style.width = '300px';
  creative.dom.expandedContent.style.left = '300px';

  //creative.dom.expandedExit.style.display = 'none';
  creative.dom.collapseButton.style.display = 'none';
  creative.dom.collapsedContent.style.display = 'block';
  creative.dom.collapsedExit.style.display = 'block';
  creative.dom.expandButton.style.display = 'block';

  // When animation finished must call
  Enabler.finishCollapse();
}

function expandFinishHandler() {
  creative.isExpanded = true;

  if (hasShifted==false){        

      document.getElementById("scene2").style.right = "0px";  
      document.getElementById("plunger_1").style.left = "390px";
      document.getElementById("plunger_2").style.left = "424px"; 
      document.getElementById("FYOR").style.left = "213px"; 
      document.getElementById("SWEIG_logo").style.left = "225px"; 

      document.getElementById("fyor_text_find").style.left = "214px"; 
      document.getElementById("fyor_text_yourown").style.left = "236px"; 
      document.getElementById("fyor_text_rhythm_r").style.left = "238px"; 
      document.getElementById("fyor_text_rhythm_h").style.left = "258px"; 
      document.getElementById("fyor_text_rhythm_y").style.left = "278px"; 
      document.getElementById("fyor_text_rhythm_t").style.left = "298px"; 
      document.getElementById("fyor_text_rhythm_hh").style.left = "318px"; 
      document.getElementById("fyor_text_rhythm_m").style.left = "340px"; 
      document.getElementById("fyor_text_underline").style.left = "242px"; 

      hasShifted = true;
    }
}

function collapseFinishHandler() {
  creative.isExpanded = false;
  if (hasShifted==true){

      document.getElementById("plunger_1").style.left = "160px";
      document.getElementById("plunger_2").style.left = "204px";
      document.getElementById("FYOR").style.left = "73px";  
      document.getElementById("SWEIG_logo").style.left = "80px"; 
      document.getElementById("fyor_text_find").style.left = "74px"; 
      document.getElementById("fyor_text_yourown").style.left = "96px"; 
      document.getElementById("fyor_text_rhythm_r").style.left = "98px"; 
      document.getElementById("fyor_text_rhythm_h").style.left = "118px"; 
      document.getElementById("fyor_text_rhythm_y").style.left = "138px"; 
      document.getElementById("fyor_text_rhythm_t").style.left = "158px"; 
      document.getElementById("fyor_text_rhythm_hh").style.left = "178px"; 
      document.getElementById("fyor_text_rhythm_m").style.left = "200px"; 
      document.getElementById("fyor_text_underline").style.left = "102px"; 

    hasShifted = false;
  }
}

function onCollapseClickHandler(){
  Enabler.requestCollapse();
  Enabler.stopTimer('Panel Expansion');
}

function onExpandHandler(){
  Enabler.requestExpand();
  Enabler.startTimer('Panel Expansion');
}

function exitClickHandler() {
  Enabler.requestCollapse();
  Enabler.stopTimer('Panel Expansion');
  Enabler.exit('BackgroundExit');
}

function collapsedExitClickHandler() {
  Enabler.exit('CollapsedExit');
}


var creative = {};


function setupDom() {
  creative.dom = {};
  creative.dom.mainContainer = document.getElementById('main-container');
  creative.dom.expandedExit = document.getElementById('expanded-exit');
  creative.dom.expandedContent = document.getElementById('expanded-state');
  creative.dom.collapsedExit = document.getElementById('collapsed-exit');
  creative.dom.collapsedContent = document.getElementById('collapsed-state');
  creative.dom.collapseButton = document.getElementById('collapse-button');
  creative.dom.expandButton = document.getElementById('expand-button');
  creative.dom.loaded_assets = document.getElementById('loaded_assets');
}

// Runs when the page is completely loaded.
function politeInit(){

  creative.dom.expandedContent.style.display = 'block';
  creative.dom.expandedExit.style.display = 'block';
  creative.dom.collapseButton.style.display = 'none';

  creative.dom.collapsedContent.style.display = 'block';
  creative.dom.collapsedExit.style.display = 'block';
  creative.dom.expandButton.style.display = 'block';

  var newImages = [];
  var imageCount = 0;
  // Order of images in array determines their order in DOM
  var imageFiles = ["scene2.jpg", "plunger_2.png", "plunger_1.png", "scene1.jpg", "ramble_r.png", "ramble_a.png","ramble_m.png","ramble_b.png","ramble_l.png","ramble_e.png","copy2a.png","copy2b.png", "copy1b.png"];


  function createImage(imageFiles){
    for(i=0; i<imageFiles.length; i++){
       newImages[i] = new Image();
         newImages[i].onload = function(){
  				 imageCount++;
  				 checkImageCount(imageCount);
         };
       newImages[i].src=imageFiles[i];
       newImages[i].id=imageFiles[i].slice(0,-4);
       creative.dom.loaded_assets.appendChild(newImages[i]);
    }

  }

  function checkImageCount(theCount){
  	if(theCount==imageFiles.length){
       // All images loaded, run the animation
        // Only for Ramble
        var rambleFiles = ["ramble_r", "ramble_a", "ramble_m", "ramble_b", "ramble_l", "ramble_e"];
        
        for(i=0; i<rambleFiles.length; i++){
            thisRambleLetter  = document.getElementById(rambleFiles[i]);
            thisRambleLetter.className += " ramble";
        }

  		 animateTL();  	
    }
  }

  if (typeof imageFiles !== 'undefined'){
  	createImage(imageFiles);
  }else{
    // No images to load, run the animation
    animateTL();
  }
  // .Image Loader
}

var loopCount = 0;

var animateTL = function() {

  var theTL = new TimelineMax();
  theTL.set(copy1b, {top: "250px",autoAlpha: 0})
  .set(scene1, {scale:"1"})
  .set(plunger_1, {top: "30px", autoAlpha:0})
  .set(plunger_2, {top: "10px", autoAlpha:0})
  .set(FYOR, {autoAlpha: 0})
  //.set(copy1a, {top: "250px", left: "8px"})

  .set([ramble_r, ramble_a, ramble_m, ramble_b, ramble_l, ramble_e], {top: "130px", autoAlpha: 1})
  .set(ramble_r, {left: "300px"})
  .set(ramble_a, {left: "340px"})
  .set(ramble_m, {left: "368px"})
  .set(ramble_b, {left: "408px"})
  .set(ramble_l, {left: "427px"})
  .set(ramble_e, {left: "451px"})
  .set(copy2b, {top: "142px",autoAlpha: 0})

  .set(SWEIG_logo, {autoAlpha: 1})
  .set(fyor_text_find, {top: "-86px"})
  .set(fyor_text_yourown, {top: "250px"})
  .set([fyor_text_rhythm_r,fyor_text_rhythm_h,fyor_text_rhythm_y,fyor_text_rhythm_t,fyor_text_rhythm_hh,fyor_text_rhythm_m], {top: "294px",autoAlpha: 1})
  .set([scene1,scene2, copy1b,fyor_text_find, fyor_text_yourown],{autoAlpha: 1})
  .set(fyor_text_underline,{autoAlpha: 1,width:"0px"})

  .staggerTo(".ramble", 3, { bezier:[{left:"-=100", top:"-=20px"},{left:"-=150", top:"+=20px"},{left:"-=200", top:"+=15px"}, {left:"-=250", top:"-=10px"}, {left:"-=295", top:"+=5px"}], ease: Sine.easeOut, y: 0 }, 0.2)  
  .to(copy1b, 1, {top:"219px"}, "-=1")
  .to([ramble_r, ramble_a, ramble_m, ramble_b, ramble_l, ramble_e,copy1b, scene1], 1, {autoAlpha: 0, delay: 1})
  .to(copy2a, 0.5, {top:"63px", autoAlpha: 1, onStart:startPlunge, ease: Expo.easeOut, y: 0})  
  .to(copy2b, 1, {autoAlpha: 1})   

  .to([plunger_1, plunger_2,copy2a,copy2b,scene2], 0.5, {autoAlpha:0, delay: 1})
  .to(fyor_text_find, 0.5, {top:"10px"})
  .to(fyor_text_yourown, 0.5, {top:"100px"})
  .staggerTo(".fyor_rhythm", 0.5, {top:"123px", ease: Elastic.easeOut.config(1, 0.8), y: 0}, 0.4)
  .to(fyor_text_underline, 0.5, {width:"124px",ease:Power4.easeIn,onComplete:completeHandler, onCompleteParams:[true]})

  function startPlunge(){
    var plungeTL = new TimelineMax();
    plungeTL.to(plunger_1, 0.5,{top:"70px", autoAlpha: 1})
    .to(plunger_2, 0.8,{top:"40px", autoAlpha: 1}, "-=0.8")
    .to([plunger_1], 3.5, {top:"150px",ease:Linear.easeNone})
    .to([plunger_2], 3.5, {top:"100px",ease:Linear.easeNone}, "-=3.5")
  }

  function completeHandler() {
     loopCount++;
      if(loopCount<2){
        var fadeAni = new TimelineMax({paused:true, onComplete:completeFadeHandler, onCompleteParams:[true]});
          fadeAni.to([fyor_text_underline, fyor_text_find, fyor_text_yourown, fyor_text_rhythm_r,fyor_text_rhythm_h,fyor_text_rhythm_y,fyor_text_rhythm_t,fyor_text_rhythm_hh,fyor_text_rhythm_m], 1, {autoAlpha: 0, delay:1});
          fadeAni.to(SWEIG_logo, 0.5, {autoAlpha: 0});
          fadeAni.restart();
      }
   }

   function completeFadeHandler(){
     theTL.pause(0);
     theTL.clear();
     animateTL();
  }
}

/**
 *  Main onload handler
 */
window.addEventListener('load', preInit);

})();